package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/uptrace/bun"
	"github.com/uptrace/bun/migrate"
	"github.com/urfave/cli/v2"
	"gitlab.com/miami25/document-store/config"
	"gitlab.com/miami25/document-store/internal/db2/migrate"
	"gitlab.com/miami25/document-store/internal/server"
)

func main() {
	app := &cli.App{
		Name: "docStore",
		Commands: []*cli.Command{
			newRunCommand(),
			newDBCommand(),
		},
	}
	if err := app.Run(os.Args); err != nil {
		log.Print(os.Args)
		log.Fatal(err)
	}
}

func newRunCommand() *cli.Command {
	return &cli.Command{
		Name:  "run",
		Usage: "run server",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "config",
				Value:    "config.yaml",
				Usage:    "path to config file",
				Required: true,
			},
		},
		Action: func(c *cli.Context) error {
			cfg := config.NewConfig(c.String("config"))
			logger, err := cfg.Logger()
			if err != nil {
				return err
			}
			defer logger.Sync()
			sugar := logger.Sugar()
			defer sugar.Sync()

			srv := server.NewServer(c.Context, cfg, sugar)

			return srv.Run(c.Context)
		},
	}
}

func newDBCommand() *cli.Command {
	var cfg *config.Config
	var db *bun.DB
	var migrator *migrate.Migrator
	return &cli.Command{
		Name:  "db",
		Usage: "database migrations",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "config",
				Value:    "config.yaml",
				Usage:    "path to config file",
				Required: true,
			},
		},
		Before: func(c *cli.Context) error {
			cfg = config.NewConfig(c.String("config"))
			var err error
			db, err = cfg.NewDB()
			if err != nil {
				return err
			}
			migrator = migrate.NewMigrator(db, migrations.Migrations)
			return nil
		},
		Subcommands: []*cli.Command{
			{
				Name:  "init",
				Usage: "create migration tables",
				Action: func(c *cli.Context) error {
					return migrator.Init(c.Context)
				},
			},
			{
				Name:  "migrate",
				Usage: "migrate database",
				Action: func(c *cli.Context) error {
					if err := migrator.Lock(c.Context); err != nil {
						return err
					}
					defer migrator.Unlock(c.Context) //nolint:errcheck

					group, err := migrator.Migrate(c.Context)
					if err != nil {
						return err
					}
					if group.IsZero() {
						fmt.Printf("there are no new migrations to run (database is up to date)\n")
						return nil
					}
					fmt.Printf("migrated to %s\n", group)
					return nil
				},
			},
			{
				Name:  "rollback",
				Usage: "rollback the last migration group",
				Action: func(c *cli.Context) error {
					if err := migrator.Lock(c.Context); err != nil {
						return err
					}
					defer migrator.Unlock(c.Context) //nolint:errcheck

					group, err := migrator.Rollback(c.Context)
					if err != nil {
						return err
					}
					if group.IsZero() {
						fmt.Printf("there are no groups to roll back\n")
						return nil
					}
					fmt.Printf("rolled back %s\n", group)
					return nil
				},
			},
			{
				Name:  "lock",
				Usage: "lock migrations",
				Action: func(c *cli.Context) error {
					return migrator.Lock(c.Context)
				},
			},
			{
				Name:  "unlock",
				Usage: "unlock migrations",
				Action: func(c *cli.Context) error {
					return migrator.Unlock(c.Context)
				},
			},
			{
				Name:  "create_go",
				Usage: "create Go migration",
				Action: func(c *cli.Context) error {
					name := strings.Join(c.Args().Slice(), "_")
					mf, err := migrator.CreateGoMigration(c.Context, name)
					if err != nil {
						return err
					}
					fmt.Printf("created migration %s (%s)\n", mf.Name, mf.Path)
					return nil
				},
			},
			{
				Name:  "create_sql",
				Usage: "create up and down SQL migrations",
				Action: func(c *cli.Context) error {
					name := strings.Join(c.Args().Slice(), "_")
					files, err := migrator.CreateSQLMigrations(c.Context, name)
					if err != nil {
						return err
					}

					for _, mf := range files {
						fmt.Printf("created migration %s (%s)\n", mf.Name, mf.Path)
					}

					return nil
				},
			},
			{
				Name:  "status",
				Usage: "print migrations status",
				Action: func(c *cli.Context) error {
					ms, err := migrator.MigrationsWithStatus(c.Context)
					if err != nil {
						return err
					}
					fmt.Printf("migrations: %s\n", ms)
					fmt.Printf("unapplied migrations: %s\n", ms.Unapplied())
					fmt.Printf("last migration group: %s\n", ms.LastGroup())
					return nil
				},
			},
			{
				Name:  "mark_applied",
				Usage: "mark migrations as applied without actually running them",
				Action: func(c *cli.Context) error {
					group, err := migrator.Migrate(c.Context, migrate.WithNopMigration())
					if err != nil {
						return err
					}
					if group.IsZero() {
						fmt.Printf("there are no new migrations to mark as applied\n")
						return nil
					}
					fmt.Printf("marked as applied %s\n", group)
					return nil
				},
			},
		},

		Action: func(c *cli.Context) error {
			cfg = config.NewConfig(c.String("config"))
			var err error
			db, err = cfg.NewDB()
			if err != nil {
				return err
			}
			defer db.Close() //nolint:errcheck
			migrator = migrate.NewMigrator(db, migrations.Migrations)

			return nil
		},
	}
}
