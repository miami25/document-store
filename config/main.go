package config

import (
	"crypto/tls"
	"crypto/x509"
	"database/sql"
	"encoding/base64"
	"fmt"
	"os"

	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
	"go.uber.org/zap"
	"gopkg.in/yaml.v3"
)

type Config struct {
	Database DatabaseConfig `yaml:"database"`
	Server   ServerConfig   `yaml:"server"`
	Log      LogConfig      `yaml:"log"`
}

type LogConfig struct {
	Level string `yaml:"level"`
}

type ServerConfig struct {
	Port string `yaml:"port"`
}

type DatabaseConfig struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Database string `yaml:"database"`
	Cert     string `yaml:"cert"`
}	

func NewConfig(filePath string) *Config {
	file, err := os.ReadFile(filePath)
	if err != nil {
		panic(err)
	}
	var config Config
	err = yaml.Unmarshal(file, &config)
	if err != nil {
		panic(err)
	}

	return &config
}

func (c *Config) NewDB() (*bun.DB, error) {
	tlsConf := &tls.Config{
		InsecureSkipVerify: true,
	}

	if c.Database.Cert != "" {
		rootCertPool := x509.NewCertPool()
		pem, err := base64.StdEncoding.DecodeString(c.Database.Cert)
		if err != nil {
			panic(err)
		}

		rootCertPool.AppendCertsFromPEM(pem)
		tlsConf = &tls.Config{
			RootCAs: rootCertPool,
			ServerName: c.Database.Host,
		}
	}
	//postgresql://[user[:password]@][netloc][:port][/dbname][?param1=value1&...]

	// dsnString := fmt.Sprintf("postgres://  ", c.Database.Host, c.Database.User, c.Database.Password, c.Database.Database, c.Database.Port)
	dsnString := ""
	if c.Database.Cert == "" {
		dsnString = fmt.Sprintf("postgres://%s:%s@%s:%s/%s", c.Database.User, c.Database.Password, c.Database.Host, c.Database.Port, c.Database.Database)
	} else {
		dsnString = fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=verify-full", c.Database.User, c.Database.Password, c.Database.Host, c.Database.Port, c.Database.Database)
	}
	sqldb := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsnString), pgdriver.WithTLSConfig(tlsConf)))
	db := bun.NewDB(sqldb, pgdialect.New())
	return db, nil
}

func (c *Config) Logger() (*zap.Logger, error) {
	switch c.Log.Level {
	case "debug":
		return zap.NewDevelopment()
	case "info":
		return zap.NewProduction()
	default:
		return zap.NewDevelopment()
	}
}
