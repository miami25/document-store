openapi: '3.0.2'
info:
  title: SCAN II Document Store
  version: 0.0.1
# We are going to be taking `Authorization: Bearer TOKEN` header for our authentication
paths:
  /document_store/v1/documents:
    get:
      summary: List documents
      operationId: listDocuments
      parameters:
      - name: owner
        in: query
        schema:
          type: string
        description: AccountID of the owner
      - name: shared
        in: query
        schema:
          type: string
        description: AccountID of the account documents were shared with
      responses:
        200:
          description: A successful response.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/documentListResource"
        401:
          description: Forbidden response;
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        default:
          description: Generic error response.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
      tags:
        - documents
    options:
      tags:
        - documents
      summary: Get all methods for documents
      responses:
        200:
          description: A successful response.
        401:
          description: Forbidden response; User has to access to the document
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        default:
          description: Generic error response.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
    post:
      tags:
        - documents
      requestBody:
        description: body of the request to create new document
        required: true
        content:
          application/json:
            schema: 
              $ref: "#/components/schemas/documentResource"
      responses:
        200: 
          description: Create new document
          content:
            application/json:
              schema: 
                $ref: "#/components/schemas/documentResource"

  /document_store/v1/documents/{id}:
    get:
      tags:
        - documents
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: string
      responses:
        200:
          description: Get document by ID
          content:
            application/json:
              schema: 
                $ref: "#/components/schemas/documentResource"
        401:
          description: Forbidden response;
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        default:
          description: Generic error response.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
    put:
      tags:
        - documents
      parameters:
      - name: id
        in: path
        required: true
        schema: 
          type: string
      requestBody:
        description: body of the request to create new document
        required: true
        content:
          application/json:
            schema: 
              $ref: "#/components/schemas/documentResource"
      responses:
        200: 
          description: Update document
          content:
            application/json:
              schema: 
                $ref: "#/components/schemas/documentResource"
        401:
          description: Forbidden response;
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        default:
          description: Generic error response.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"

  /document_store/v1/statistics/document_count/{owner}:
    get:
      tags:
        - statistics
      parameters:
      - name: owner
        in: path
        required: true
        schema:
          type: string
      responses:
        200:
          description: Get document count by owner
          content:
            application/json:
              schema: 
                $ref: "#/components/schemas/documentCountResource"
        401:
          description: Forbidden response;
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        default:
          description: Generic error response.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
components:
  schemas:
    documentCountResource:
      type: object
      required:
        - data
        - links
        - metadata
      properties: 
        data: 
          $ref: "#/components/schemas/documentCount"
        links:
          $ref: "#/components/schemas/links"
        metadata: 
          $ref: "#/components/schemas/metadata"
    documentCount:
      type: object
      required:
        - owner
        - count
        - id
        - type
      properties:
        id:
          type: string
        owner:
          type: string
        count:
          type: integer
        type:
          type: string
    document:
      type: object
      required:
        - id
        - type
        - uuid
        - documentHash
        - signature
        - owner
      properties:
        id:
          type: string
        type:
          type: string
        uuid:
          type: string
        documentHash:
          type: string
          description: "Hash of the document"
        signature:
          type: string
        owner:
          type: string
        documentDetails:
          $ref: "#/components/schemas/documentDetails"
    documentDetails:
      type: object
      properties: 
        createdAt:
          type: integer
        updatedAt:
          type: integer
        blockHash:
          type: string
        blockIndex:
          type: integer
    error:
      type: object
      required:
        - message
        - detailedMessage
        - id 
        - type
      properties:
        id:
          type: string
        type:
          type: string
        code:
          type: integer
          format: int32
        message:
          type: string
        detailedMessage:
          type: string
    documentListResource:
      type: object
      required:
        - data
        - links
        - metadata
      properties: 
        data: 
          type: array
          items: 
            $ref: "#/components/schemas/document"
        links:
          $ref: "#/components/schemas/links"
        metadata: 
          $ref: "#/components/schemas/metadata"
    documentResource:
      type: object
      required:
        - data
        - links
        - metadata
      properties: 
        data:
          $ref: "#/components/schemas/document"
        links:
          $ref: "#/components/schemas/links"
        metadata: 
          $ref: "#/components/schemas/metadata"
    links: 
      type: object
      required:
        - self
      properties: 
        self:
          type: string
          description: link to the current resource
        next: 
          type: string
          description: link to the next page of resources
        prev: 
          type: string
          description: link to the previous page of resources
    metadata:
      type: object
      description: generic free form metadata object
          
