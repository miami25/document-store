package lib

import (
	"time"

	"gitlab.com/miami25/document-store/resources"
)

type ResourceType string

const (
	DocumentType      ResourceType = "document"
	DocumentCountType ResourceType = "document_count"
)

type Document struct {
	ID              uint64          `json:"id"`
	Type            string          `json:"type"`
	UUId            string          `json:"uuid"`
	DocumentHash    string          `json:"document_hash"`
	Title           string          `json:"title"`
	Owner           string          `json:"owner"`
	Signature       string          `json:"signature"`
	DocumentDetails DocumentDetails `json:"details"`
}

type DocumentDetails struct {
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
}

type DocumentResource struct {
	Data  Document        `json:"data"`
	Links resources.Links `json:"links"`
}

type DocumentListResource struct {
	Data  []Document      `json:"data"`
	Links resources.Links `json:"links"`
}

type DocumentCount struct {
	Count int    `json:"count"`
	Type  string `json:"type"`
	Owner string `json:"owner"`
	ID    string `json:"id"`
}

type DocumentCountResource struct {
	Data  DocumentCount   `json:"data"`
	Links resources.Links `json:"links"`
}

type DocumentID struct {
	ID   uint64 `json:"id"`
	Type string `json:"type"`
}

type DocumentIDResource struct {
	Data  DocumentID      `json:"data"`
	Links resources.Links `json:"links"`
}

type DocumentIDListResource struct {
	Data  []DocumentID    `json:"data"`
	Links resources.Links `json:"links"`
}
