module gitlab.com/miami25/document-store

go 1.19

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/go-chi/chi/v5 v5.0.8
	github.com/uptrace/bun v1.1.11
	github.com/uptrace/bun/dialect/pgdialect v1.1.11
	github.com/uptrace/bun/driver/pgdriver v1.1.11
	go.uber.org/zap v1.24.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/tmthrgd/go-hex v0.0.0-20190904060850-447a3041c3bc // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.5 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/goleak v1.1.12 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	mellium.im/sasl v0.3.1 // indirect
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/urfave/cli/v2 v2.24.4
	golang.org/x/crypto v0.6.0 // indirect
)
