package db2_test

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"errors"
	"fmt"
	"log"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/miami25/document-store/internal/db2"
)

type CustomConverter struct{}

func (s CustomConverter) ConvertValue(v interface{}) (driver.Value, error) {
	switch v.(type) {
	case uint64:
		return v.(uint64), nil
	case time.Time:
		return v.(time.Time), nil
	case int64:
		return v.(int64), nil
	case string:
		return v.(string), nil
	case []string:
		return v.([]string), nil
	case int:
		return v.(int), nil
	default:
		return nil, errors.New(fmt.Sprintf("cannot convert %T with value %v", v, v))
	}
}

func NewMock() (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New(sqlmock.ValueConverterOption(CustomConverter{}))
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return db, mock
}

func TestGetDocumentByID(t *testing.T) {
	db, mock := NewMock()
	defer db.Close()

	instance := db2.NewDBFromConnection(db)

	rows := mock.NewRows([]string{"id", "uuid", "document_hash", "title", "owner", "signature", "created_at", "updated_at"}).
		AddRow(uint64(1), "uuid", "document_hash", "title", "owner", "signature", time.Now().UTC(), time.Now().UTC())

	mock.ExpectQuery("SELECT").WillReturnRows(rows)

	_, err := instance.GetDocumentByID(context.Background(), 1)
	if err != nil {
		t.Errorf("error was not expected while fetching document: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetDocumentByUUID(t *testing.T) {
	db, mock := NewMock()
	defer db.Close()

	instance := db2.NewDBFromConnection(db)

	rows := mock.NewRows([]string{"id", "uuid", "document_hash", "title", "owner", "signature", "created_at", "updated_at"}).
		AddRow(uint64(1), "uuid", "document_hash", "title", "owner", "signature", time.Now().UTC(), time.Now().UTC())

	mock.ExpectQuery("SELECT").WillReturnRows(rows)

	_, err := instance.GetDocumentByUUID(context.Background(), "uuid")
	if err != nil {
		t.Errorf("error was not expected while fetching document: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}

}

func TestGetDocumentsByOwner(t *testing.T) {
	db, mock := NewMock()
	defer db.Close()

	instance := db2.NewDBFromConnection(db)
	rows := mock.NewRows([]string{"id", "uuid", "document_hash", "title", "owner", "signature", "created_at", "updated_at" }).
		AddRow(uint64(1), "uuid", "document_hash", "title", "owner", "signature", time.Now().UTC(), time.Now().UTC())

	mock.ExpectQuery("SELECT").WillReturnRows(rows)

	_, err := instance.GetDocumentsByOwner(context.Background(), "owner")
	if err != nil {
		t.Errorf("error was not expected while fetching document: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestDocumentFilters(t *testing.T) {
	db, mock := NewMock()
	defer db.Close()

	instance := db2.NewDBFromConnection(db)
	rows := mock.NewRows([]string{"id", "uuid", "document_hash", "title", "owner", "signature", "created_at", "updated_at"}).
		AddRow(uint64(1), "uuid", "document_hash", "title", "owner", "signature", time.Now().UTC(), time.Now().UTC())

	mock.ExpectQuery("SELECT").WillReturnRows(rows)

	_, err := instance.Query().FilterByOwner("owner").GetDocuments(context.Background())
	if err != nil {
		t.Errorf("error was not expected while fetching document: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}
