SET statement_timeout = 0;


--bun:split

CREATE TABLE if not exists docs (
    id bigserial primary key,
    uuid varchar(255) not null,
    document_hash varchar(255) not null,
    title varchar(255) not null,
    owner varchar(255) not null,
    signature varchar(255) not null,
    created_at timestamptz not null default current_timestamp,
    updated_at timestamptz not null default current_timestamp,
);

