package db2

import (
	"time"

	"github.com/uptrace/bun"
	"gitlab.com/miami25/document-store/lib"
)

// Document struct
type Document struct {
	bun.BaseModel `bun:"table:docs,alias:d"`
	ID            uint64    `bun:"id,pk,autoincrement"`
	UUId          string    `bun:"uuid"`
	DocumentHash  string    `json:"document_hash"`
	Title         string    `json:"title"`
	Owner         string    `json:"owner"`
	Signature     string    `json:"signature"`
	CreatedAt     time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	UpdatedAt     time.Time `bun:",nullzero,notnull,default:current_timestamp"`
}

func NewDocument(d lib.Document) Document {
	return Document{
		UUId:         d.UUId,
		DocumentHash: d.DocumentHash,
		Title:        d.Title,
		Owner:        d.Owner,
		Signature:    d.Signature,
		CreatedAt:    d.DocumentDetails.CreatedAt,
		UpdatedAt:    d.DocumentDetails.UpdatedAt,
	}
}

func (d Document) Update(libDoc lib.Document) Document {
	d.DocumentHash = libDoc.DocumentHash
	d.Title = libDoc.Title
	d.Owner = libDoc.Owner
	d.Signature = libDoc.Signature
	d.UpdatedAt = time.Now()
	return d
}

func (d Document) ToResourceDocument() lib.Document {
	return lib.Document{
		Title:        d.Title,
		Type:         string(lib.DocumentType),
		Owner:        d.Owner,
		ID:           d.ID,
		DocumentHash: d.DocumentHash,
		Signature:    d.Signature,
		UUId:         d.UUId,
		DocumentDetails: lib.DocumentDetails{
			CreatedAt:  d.CreatedAt,
			UpdatedAt:  d.UpdatedAt,
		},
	}
}

func DocumentSlice(dbDocuments []Document) []lib.Document {
	documents := make([]lib.Document, len(dbDocuments))
	for i, document := range dbDocuments {
		documents[i] = document.ToResourceDocument()
	}
	return documents
}

func DBDocumentSlice(documents []lib.Document) []Document {
	dbDocuments := make([]Document, len(documents))
	for i, document := range documents {
		dbDocuments[i] = NewDocument(document)
	}
	return dbDocuments
}
