package db2

import (
	"context"

	"database/sql"

	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"gitlab.com/miami25/document-store/config"
	"gitlab.com/miami25/document-store/resources"
)

type DB struct {
	bun   *bun.DB
	query *bun.SelectQuery
}

type DocumentDB interface {
	GetDocumentByID(ctx context.Context, id uint64) (*Document, error)
	GetDocumentByHash(ctx context.Context, hash string) (*Document, error)
	GetDocumentsByOwner(ctx context.Context, owner string) ([]Document, error)
	GetDocumentByUUID(ctx context.Context, uuid string) (*Document, error)
	CreateDocument(ctx context.Context, document *Document) (*Document, error)
	CreateDocumentBatch(ctx context.Context, documents []Document) ([]Document, error)
	UpdateDocument(ctx context.Context, document *Document) (*Document, error)
	DeleteDocument(ctx context.Context, id uint64) error
	Query() DocumentDB
	Count(ctx context.Context) (int, error)
	FilterByOwner(owner string) DocumentDB
	ApplyFilters(filters resources.Filters) DocumentDB
	GetDocuments(ctx context.Context) ([]Document, error)
	Page(number int, limit int, order resources.Order) DocumentDB
}

func NewDB(cfg *config.Config) *DB {
	db, err := cfg.NewDB()
	if err != nil {
		panic(err)
	}
	return &DB{
		bun: db,
	}
}

func (db *DB) Ping(ctx context.Context) error {
	return db.bun.Ping()
}

func NewDBFromConnection(db *sql.DB) *DB {

	return &DB{
		bun: bun.NewDB(db, pgdialect.New()),
	}
}

func (db *DB) GetDocumentByID(ctx context.Context, id uint64) (*Document, error) {
	var document Document
	err := db.bun.NewSelect().Model(&document).Where("id = ?", id).Scan(ctx, &document)

	if err != nil {
		return nil, err
	}
	return &document, nil
}

func (db *DB) GetDocumentsByOwner(ctx context.Context, owner string) ([]Document, error) {
	var documents []Document
	err := db.bun.NewSelect().Model(&documents).Where("owner = ?", owner).Scan(ctx, &documents)

	if err != nil {
		return nil, err
	}
	return documents, nil
}

func (db *DB) GetDocumentByHash(ctx context.Context, hash string) (*Document, error) {
	var document Document
	err := db.bun.NewSelect().Model(&document).Where("hash = ?", hash).Scan(ctx, &document)

	if err != nil {
		return nil, err
	}
	return &document, nil
}
func (db *DB) GetDocumentByUUID(ctx context.Context, uuid string) (*Document, error) {
	var document Document
	err := db.bun.NewSelect().Model(&document).Where("uuid = ?", uuid).Scan(ctx, &document)

	if err != nil {
		return nil, err
	}
	return &document, nil
}

func (db *DB) CreateDocument(ctx context.Context, document *Document) (*Document, error) {

	_, err := db.bun.NewInsert().Model(document).Exec(ctx)
	if err != nil {
		return nil, err
	}

	if err != nil {
		return nil, err
	}
	return document, nil
}

func (db *DB) CreateDocumentBatch(ctx context.Context, documents []Document) ([]Document, error) {
	_, err := db.bun.NewInsert().Model(&documents).Exec(ctx)
	if err != nil {
		return nil, err
	}
	return documents, nil
}

func (db *DB) UpdateDocument(ctx context.Context, document *Document) (*Document, error) {
	_, err := db.bun.NewUpdate().Model(document).Exec(ctx)
	if err != nil {
		return nil, err
	}
	return document, nil
}

func (db *DB) DeleteDocument(ctx context.Context, id uint64) error {
	_, err := db.bun.NewDelete().Model(&Document{}).Where("id = ?", id).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (db *DB) Query() DocumentDB {
	db.query = db.bun.NewSelect()
	return db
}
func (db *DB) FilterByOwner(owner string) DocumentDB {
	db.query = db.query.Where("owner = ?", owner)
	return db
}

// page with last id & page limit
func (db *DB) Page(number int, limit int, order resources.Order) DocumentDB {
	offset := (number - 1) * limit
	if order == resources.OrderAsc {
		db.query = db.query.Order("id ASC").Limit(limit).Offset(offset)
	} else {
		db.query = db.query.Order("id DESC").Limit(limit).Offset(offset)
	}
	return db
}

func (db *DB) Count(ctx context.Context) (int, error) {
	count, err := db.query.Model((*Document)(nil)).Count(ctx)
	if err != nil {
		return 0, err
	}
	return count, nil
}

func (db *DB) ApplyFilters(filters resources.Filters) DocumentDB {
	if filters.Owner != nil {
		db.FilterByOwner(*filters.Owner)
	}
	return db
}

// select with query
func (db *DB) GetDocuments(ctx context.Context) ([]Document, error) {
	documents := []Document{}
	err := db.query.Model(&documents).Scan(ctx, &documents)
	if err != nil {
		return nil, err
	}

	return documents, nil
}
