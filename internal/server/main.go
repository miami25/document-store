package server

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/miami25/document-store/config"
	"gitlab.com/miami25/document-store/internal/db2"
	"gitlab.com/miami25/document-store/internal/server/handlers"
	"gitlab.com/miami25/document-store/internal/server/mdw"
	"go.uber.org/zap"
)

type Server struct {
	Router *chi.Mux
	cfg    *config.Config
	db     *db2.DB
	log    *zap.SugaredLogger
}

func NewServer(ctx context.Context, cfg *config.Config, logger *zap.SugaredLogger) *Server {
	logger.Info("Server init started", zap.String("port", cfg.Server.Port))

	r := chi.NewRouter()

	db := db2.NewDB(cfg)
	if err := db.Ping(ctx); err != nil {
		logger.Fatal("Failed to ping database", zap.Error(err))
	}

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(mdw.PutConfigInContext(cfg))
	r.Use(mdw.PutDBInContext(db))
	r.Use(mdw.PutLoggerInContext(logger))

	r.Use(middleware.Timeout(60 * time.Second))
	r.Route("/document_store/v1", func(r chi.Router) {
		r.Route("/statistics", func(r chi.Router) {
			r.Get("/document_count/{owner}", handlers.GetDocumentCountHandler)
		})

		r.Route("/documents", func(r chi.Router) {
			r.Get("/", handlers.GetDocumentListHandler)
			r.Post("/", handlers.CreateDocumentHandler)
			r.Post("/batch", handlers.CreateDocumentBatchHandler)
			r.Get("/{documentID}", handlers.GetDocumentHandler)
			//   r.Put("/{documentID}", handlers.UpdateDocumentHandler)
			//   r.Delete("/{documentID}", handlers.DeleteDocumentHandler)
		})

		r.Get("/health", handlers.GetHealthHandler)
	})

	logger.Info("Init finished", zap.String("port", cfg.Server.Port))
	return &Server{
		Router: r,
		cfg:    cfg,
		db:     db,
		log:    logger,
	}
}

func (s *Server) Run(ctx context.Context) error {
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", s.cfg.Server.Port),
		Handler: s.Router,
		BaseContext: func(listener net.Listener) context.Context {
			return ctx
		},
	}
	s.log.Info("Server started", zap.String("port", s.cfg.Server.Port))
	return srv.ListenAndServe()
}
