package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/miami25/document-store/internal/server/mdw"
	"gitlab.com/miami25/document-store/lib"
)

func GetDocumentCountHandler(w http.ResponseWriter, r *http.Request) {
	log := mdw.GetLoggerFromContext(r)
	log.Debug("GetDocumentListHandler")
	db := mdw.GetDBFromContext(r)

	owner := chi.URLParam(r, "owner")
	if owner == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid request"))
		return
	}

	count, err := db.Query().FilterByOwner(owner).Count(r.Context())
	if err != nil {
		log.Debug(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal server error"))
		return
	}

	response := lib.DocumentCountResource{}
	response.Data.Count = count
	response.Data.Owner = owner
	response.Data.Type = string(lib.DocumentCountType)
	response.Data.ID = owner

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}
