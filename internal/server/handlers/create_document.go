package handlers

import (
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/miami25/document-store/internal/db2"
	"gitlab.com/miami25/document-store/internal/server/mdw"
	"gitlab.com/miami25/document-store/lib"
)

func CreateDocumentHandler(w http.ResponseWriter, r *http.Request) {
	log := mdw.GetLoggerFromContext(r)
	log.Debug("CreateDocumentHandler")
	log.Debugf("Request: %s", r.URL.Path)

	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid request"))
		return
	}
	log.Debug(body)

	documentReq := lib.DocumentResource{}
	err = json.Unmarshal(body, &documentReq)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid request"))
		return
	}

	dbDoc := db2.NewDocument(documentReq.Data)

	db := mdw.GetDBFromContext(r)

	// Check if the document already exists
	_, err = db.GetDocumentByHash(r.Context(), dbDoc.DocumentHash)
	if err == nil {
		w.WriteHeader(http.StatusConflict)
		w.Write([]byte("Document already exists"))
		return
	}

	//Create a new document
	doc, err := db.CreateDocument(r.Context(), &dbDoc)
	if err != nil {
		log.Debug(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal server error"))
		return
	}

	resp := lib.DocumentResource{}
	resp.Data = doc.ToResourceDocument()

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(resp)

}
