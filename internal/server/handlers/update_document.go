package handlers

import (
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/miami25/document-store/internal/db2"
	"gitlab.com/miami25/document-store/internal/server/mdw"
	"gitlab.com/miami25/document-store/lib"
)

func UpdateDocumentHandler(w http.ResponseWriter, r *http.Request) {
	log := mdw.GetLoggerFromContext(r)
	log.Debug("UpdateDocumentHandler")
	log.Debugf("Request: %s", r.URL.Path)

	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid request"))
		return
	}
	log.Debug(body)

	documentReq := lib.DocumentResource{}
	err = json.Unmarshal(body, &documentReq)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid request"))
		return
	}
	db := mdw.GetDBFromContext(r)

	// Check if the document already exists
	doc, err := db.GetDocumentByID(r.Context(), documentReq.Data.ID)
	if err != nil {
		log.Debug(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal server error"))
		return
	}
	if doc == nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Document not found"))
		return
	}

	// Update the document
	dbDoc := db2.NewDocument(documentReq.Data)
	newDoc, err := db.UpdateDocument(r.Context(), &dbDoc)
	if err != nil {
		log.Debug(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal server error"))
		return
	}

	resp := lib.DocumentResource{}
	resp.Data = newDoc.ToResourceDocument()

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
}
