package handlers

import (
	"net/http"

	"gitlab.com/miami25/document-store/internal/server/mdw"
)

func GetHealthHandler(w http.ResponseWriter, r *http.Request) {
	log := mdw.GetLoggerFromContext(r)
	log.Debug("GetHealthHandler")

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("OK"))
}
