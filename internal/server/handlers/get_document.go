package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"gitlab.com/miami25/document-store/internal/server/mdw"
	"gitlab.com/miami25/document-store/lib"
)

func GetDocumentHandler(w http.ResponseWriter, r *http.Request) {
	log := mdw.GetLoggerFromContext(r)
	log.Debug("GetDocumentHandler")

	documentID, err := strconv.ParseUint(chi.URLParam(r, "documentID"), 10, 64)
	if err != nil {
		log.Debug(err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid request"))
		return
	}

	db := mdw.GetDBFromContext(r)

	document, err := db.GetDocumentByID(r.Context(), documentID)
	if err != nil {
		log.Debug(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal server error"))
		return
	}

	if document == nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Document not found"))
		return
	}

	response := lib.DocumentResource{}
	response.Data = document.ToResourceDocument()

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)

}
