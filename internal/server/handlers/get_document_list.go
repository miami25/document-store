package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/miami25/document-store/internal/db2"
	"gitlab.com/miami25/document-store/internal/server/mdw"
	"gitlab.com/miami25/document-store/lib"
	"gitlab.com/miami25/document-store/resources"
)

func GetDocumentListHandler(w http.ResponseWriter, r *http.Request) {
	log := mdw.GetLoggerFromContext(r)
	log.Debug("GetDocumentListHandler")
	db := mdw.GetDBFromContext(r)

	filters := resources.Filters{}
	// Get the query parameters
	if r.URL.Query().Has("filter[owner]") {
		owner := r.URL.Query().Get("filter[owner]")
		filters.Owner = &owner
	}

	err := filters.Validate()
	if err != nil {
		log.Debug(err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid request"))
		return
	}

	page, err := resources.ParsePaginationParams(r.URL.Query())
	if err != nil {
		log.Debug(err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid request"))
		return
	}

	documents, err := db.Query().ApplyFilters(filters).Page(page.Number, page.Size, page.Order).GetDocuments(r.Context())
	if err != nil {
		log.Debug(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal server error"))
		return
	}

	response := lib.DocumentListResource{}
	response.Data = db2.DocumentSlice(documents)

	paginator := resources.NewPaginator("/document_store/v1/documents", page.Size, page.Order, page.Number, filters)
	response.Links = paginator.Links()

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}
