package handlers

import (
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/miami25/document-store/internal/db2"
	"gitlab.com/miami25/document-store/internal/server/mdw"
	"gitlab.com/miami25/document-store/lib"
)

func CreateDocumentBatchHandler(w http.ResponseWriter, r *http.Request) {
	log := mdw.GetLoggerFromContext(r)
	log.Debug("CreateDocumentBatchHandler")
	log.Debugf("Request: %s", r.URL.Path)

	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid request"))
		return
	}

	documentBatchReq := lib.DocumentListResource{}
	err = json.Unmarshal(body, &documentBatchReq)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid request"))
		return
	}

	dbDoc := db2.DBDocumentSlice(documentBatchReq.Data)

	db := mdw.GetDBFromContext(r)

	//Create new document batch
	newDocs, err := db.CreateDocumentBatch(r.Context(), dbDoc)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal server error"))
		return
	}

	response := lib.DocumentIDListResource{
		Data: make([]lib.DocumentID, len(newDocs)),
	}
	for _, d := range newDocs {
		response.Data = append(response.Data, lib.DocumentID{
			ID:   d.ID,
			Type: "document",
		})
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(response)

}
