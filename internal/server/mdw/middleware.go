package mdw

import (
	"context"
	"net/http"

	"gitlab.com/miami25/document-store/config"
	"gitlab.com/miami25/document-store/internal/db2"
	"go.uber.org/zap"
)

func KVContext(next http.Handler, key string, value interface{}) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), key, value)
		next.ServeHTTP(rw, r.WithContext(ctx))
	})
}

func GetDBFromContext(r *http.Request) db2.DocumentDB {
	return r.Context().Value("db").(db2.DocumentDB)
}

func GetConfigFromContext(r *http.Request) *config.Config {
	return r.Context().Value("config").(*config.Config)
}

func GetLoggerFromContext(r *http.Request) *zap.SugaredLogger {
	return r.Context().Value("logger").(*zap.SugaredLogger)
}

func PutLoggerInContext(logger *zap.SugaredLogger) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return KVContext(h, "logger", logger)
	}
}

func PutDBInContext(db db2.DocumentDB) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return KVContext(h, "db", db)
	}
}

func PutConfigInContext(config *config.Config) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return KVContext(h, "config", config)
	}
}
