package resources_test

import (
	"encoding/json"
	"fmt"
	"testing"

	"gitlab.com/miami25/document-store/resources"
)

// test pagination marshalling

func TestPaginationMarshall(t *testing.T) {
	p := resources.Pagination{
		Size:   10,
		Order:  resources.OrderDesc,
		Number: 10,
	}
	b, err := json.Marshal(p)
	if err != nil {
		t.Error(err)
	}
	fmt.Println(string(b))
}

func TestPaginationUnmarshall(t *testing.T) {
	p := resources.Pagination{}
	err := json.Unmarshal([]byte(`{"size":10,"order":"desc","number":10}`), &p)
	if err != nil {
		t.Error(err)
	}
	fmt.Println(p)
}
