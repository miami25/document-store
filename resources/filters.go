package resources

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
)

const (
	PageSizeQueryParam   = "page[size]"
	PageOrderQueryParam  = "page[order]"
	PageLastQueryParam   = "page[last]"
	PageNumberQueryParam = "page[number]"
)

type Filters struct {
	Owner *string `json:"owner"`
}

func (f Filters) String() string {
	result := ""
	if f.Owner != nil {
		if result != "" {
			result = result + "&"
		}
		result = result + fmt.Sprintf("filter[owner]=%s", *f.Owner)
	}
	return result
}

func (f Filters) Validate() error {
	// fail if owner is empty
	if f.Owner == nil || (f.Owner != nil && *f.Owner == "") {
		return fmt.Errorf("owner filter cannot be empty")
	}

	return nil
}

type Order int // 0 = asc, 1 = desc

const (
	OrderAsc Order = iota
	OrderDesc
)

func (o Order) Reverse() Order {
	if o == OrderAsc {
		return OrderDesc
	}
	return OrderAsc
}

// jsonenum
func (o Order) String() string {
	return [...]string{"asc", "desc"}[o]
}

func OrderFromString(s string) (Order, error) {
	switch s {
	case "asc":
		return OrderAsc, nil
	case "desc":
		return OrderDesc, nil
	default:
		return OrderAsc, fmt.Errorf("invalid order: %s", s)
	}
}

// jsonenum scan from string, value to string
func (o *Order) Scan(value interface{}) error {
	switch value.(string) {
	case "asc":
		*o = OrderAsc
	case "desc":
		*o = OrderDesc
	default:
		return fmt.Errorf("invalid order: %s", value)
	}
	return nil
}

func (o Order) Value() (driver.Value, error) {
	return o.String(), nil
}

// order marshall & unmarshall
func (o Order) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`"%s"`, o.String())), nil
}

func (o *Order) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	return o.Scan(s)
}

type Pagination struct {
	Size   int   `json:"size"`
	Order  Order `json:"order"`
	Number int   `json:"number"`
}

// pagination to string
func (p Pagination) String() string {
	return fmt.Sprintf("page[number]=%d&page[size]=%d&page[order]=%s", p.Number, p.Size, p.Order)
}
func PaginationString(size int, order Order, number int) string {
	return fmt.Sprintf("page[number]=%d&page[size]=%d&page[order]=%s", number, size, order)
}

// parse pagination params from url.Values
func ParsePaginationParams(values url.Values) (Pagination, error) {
	p := Pagination{
		Size:   10,
		Order:  OrderAsc,
		Number: 1,
	}
	// size
	if values.Get(PageSizeQueryParam) != "" {
		size, err := strconv.Atoi(values.Get(PageSizeQueryParam))
		if err != nil {
			return p, err
		}
		p.Size = size
	}
	// order
	if values.Get(PageOrderQueryParam) != "" {
		order, err := OrderFromString(values.Get(PageOrderQueryParam))
		if err != nil {
			return p, err
		}
		p.Order = order
	}
	// number
	if values.Get(PageNumberQueryParam) != "" {
		number, err := strconv.Atoi(values.Get(PageNumberQueryParam))
		if err != nil {
			return p, err
		}
		p.Number = number
	}

	return p, nil
}
