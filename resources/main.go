package resources

import (
	"fmt"
)

type Links struct {
	Self  string  `json:"self"`
	Next  *string `json:"next"`
	Prev  *string `json:"prev"`
	First *string `json:"first"`
	Last  *string `json:"last"`
}
type Paginator struct {
	Endpoint   string
	Pagination Pagination
	Filters    Filters
}

func NewPaginator(endpoint string, size int, order Order, page int, filters Filters) Paginator {
	return Paginator{
		Endpoint: endpoint,
		Pagination: Pagination{
			Size:   size,
			Order:  order,
			Number: page,
		},
		Filters: filters,
	}
}

func (p Paginator) Links() Links {
	links := Links{
		Self:  *p.Self(),
		Next:  p.Next(),
		Prev:  p.Prev(),
		First: p.First(),
	}

	return links
}

func (p Paginator) Self() *string {
	return p.linkFromPagination()
}

func (p Paginator) linkFromPagination() *string {
	newLink := fmt.Sprintf("%s?%s", p.Endpoint, p.Pagination.String())
	if p.Filters.String() != "" {
		newLink = fmt.Sprintf("%s&%s", newLink, p.Filters.String())
	}
	return &newLink
}

func (p Paginator) Next() *string {
	p.Pagination.Number++
	return p.linkFromPagination()
}

func (p Paginator) Prev() *string {
	if p.Pagination.Number > 1 {
		p.Pagination.Number--
	}
	return p.linkFromPagination()
}

func (p Paginator) First() *string {
	p.Pagination.Number = 1
	return p.linkFromPagination()
}
