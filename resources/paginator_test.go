package resources_test

import (
	"testing"

	"gitlab.com/miami25/document-store/resources"
)

// test Paginator links calculation

func TestLinks(t *testing.T) {
	// test links calculation
	paginator := resources.NewPaginator("/document_store/v1/documents", 10, resources.OrderAsc, 1, resources.Filters{})
	links := paginator.Links()
	if links.Self != "/document_store/v1/documents?page[number]=1&page[size]=10&page[order]=asc" {
		t.Errorf("Self link is wrong: %s", links.Self)
	}
	if *links.Next != "/document_store/v1/documents?page[number]=2&page[size]=10&page[order]=asc" {
		t.Errorf("Next link is wrong: %s", *links.Next)
	}
	if *links.Prev != "/document_store/v1/documents?page[number]=1&page[size]=10&page[order]=asc" {
		t.Errorf("Prev link is wrong: %s", *links.Prev)
	}
	if *links.First != "/document_store/v1/documents?page[number]=1&page[size]=10&page[order]=asc" {
		t.Errorf("First link is wrong: %s", *links.First)
	}
	if links.Last != nil {
		t.Errorf("Last link is wrong: %s", *links.Last)
	}

}
