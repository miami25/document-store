FROM golang:1.19-alpine

WORKDIR /app
COPY go.mod ./
COPY go.sum ./

RUN go mod download

COPY . ./

RUN go build -o ./docs
EXPOSE 3000

CMD [ "/app/docs", "run", "--config",  "/app/config/config.yaml" ]
